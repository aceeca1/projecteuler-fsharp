module Divisor
let ofNum(n) = seq {
    let mutable i = 1
    while i * i < n do
        if n % i = 0 then
            yield n / i
            yield i
        i <- i + 1
    if i * i = n then yield i
}
let sum(n) = (ofNum(n) |> Seq.sum) - n
let rec gcd(a, b) = if b = 0 then a else gcd(b, a % b)

module Graph
type Edge<'c>(t: int, c: 'c) =
    member u.T = t
    member u.C = c
type Vertex<'c>() =
    member val E = ResizeArray<Edge<'c>>()
type Graph<'c>() =
    member val V = ResizeArray<Vertex<'c>>()
type Builder<'c, 'v when 'v: equality>() =
    let g = Graph<'c>()
    let a = System.Collections.Generic.Dictionary<'v, int>()
    member u.AddVertexR() =
        g.V.Add(Vertex<'c>()) |> ignore
        g.V.Count - 1
    member u.AddVertex(v) =
        if a.ContainsKey(v) then a.[v] else
            a.Add(v, g.V.Count) |> ignore
            u.AddVertexR()
    member u.AddEdgeR(no1, no2, c) =
        g.V.[no1].E.Add(Edge<'c>(no2, c))
    member u.AddEdge(v1, v2, c) =
        u.AddEdgeR(u.AddVertex(v1), u.AddVertex(v2), c)
    member u.G = g
let shortPath(g: Graph<int>, no) =
    let ret = Array.create(g.V.Count)(System.Int32.MaxValue)
    ret.[no] <- 0
    let q = System.Collections.Generic.Queue<int>()
    let inQ = System.Collections.BitArray(g.V.Count)
    q.Enqueue(no)
    inQ.[no] <- true
    while q.Count <> 0 do
        let qH = q.Dequeue()
        inQ.[qH] <- false
        for i in g.V.[qH].E do
            let dis = ret.[qH] + i.C
            if dis < ret.[i.T] then
                ret.[i.T] <- dis
                if not inQ.[i.T] then
                    inQ.[i.T] <- true
                    q.Enqueue(i.T)
    ret

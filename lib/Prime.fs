module Prime
let sieve(n: int) =
    let ret = Array.zeroCreate<int>(n + 1)
    let mutable u = 0
    for i = 2 to n do
        let mutable v = ret.[i]
        if v = 0 then v <- i; ret.[u] <- i; u <- i
        let mutable w = 2
        while w > 0 && i * w <= n do
            ret.[i * w] <- w
            if w >= v then w <- -1 else w <- ret.[w]
    ret.[u] <- n + 1
    ret
type Sieve() =
    static let mutable a = sieve(1048576)
    static member doubleA() = a <- sieve((a.Length - 1) <<< 1)
    static member ensure(n) = while n >= a.Length do Sieve.doubleA()
    static member A = a
let is(n) = n >= 2 && (Sieve.ensure(n); Sieve.A.[n] > n)
let isL(n) =
    n >= 2 &&
    let m = n |> float |> sqrt |> int
    Sieve.ensure(m)
    (seq {
        let mutable i = 2
        while i <= m do
            yield i
            i <- Sieve.A.[i]
    }) |> Seq.forall(fun i -> n % i <> 0)
let next(n) = Sieve.ensure(n * 6 / 5); Sieve.A.[n]
let factorize(n) =
    Sieve.ensure(n)
    let ret = ResizeArray<int>()
    let rec f(n) =
        let p = Sieve.A.[n]
        if p > n then
            ret.Add(n)
            ret
        else
            ret.Add(p)
            f(n / p)
    if n = 1 then ret else f(n)

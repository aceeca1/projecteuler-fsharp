module Phi
let compute(a: int[]) =
    let ret = Array.zeroCreate<int>(a.Length)
    ret.[1] <- 1
    for i = 2 to a.Length - 1 do
        let p = min(i)(a.[i])
        let r = i / p
        ret.[i] <- if r % p = 0 then ret.[r] * p else ret.[r] * (p - 1)
    ret

module Matrix
let inline multiply(m1: ^a[,], m2: ^a[,]) =
    let u = m1.GetLength(0)
    let v = m1.GetLength(1)
    if v <> m2.GetLength(0) then failwith "bad input"
    let w = m2.GetLength(1)
    let ret = Array2D.zeroCreate<'a>(u)(w)
    for i = 0 to u - 1 do
        for j = 0 to v - 1 do
            for k = 0 to w - 1 do
                ret.[i, k] <- ret.[i, k] + m1.[i, j] * m2.[j, k]
    ret

module Binary
let count14(n: int) =
    let n1 = int64 n * 0x200040008001L
    let n2 = n1 &&& 0x111111111111111L
    n2 % 0xfL |> int
let deBruijn = [|
    0; 1; 28; 2; 29; 14; 24; 3; 30; 22; 20; 15; 25; 17; 4; 8
    31; 27; 13; 23; 21; 19; 16; 7; 26; 12; 18; 6; 11; 5; 10; 9|]
let which(n: int) =
    let n1 = uint32(n * 0x077CB531) >>> 27
    deBruijn.[int n1]

module Fibonacci

let inline startsFrom(a1, a2) = seq {
    let mutable b1 = a1
    let mutable b2 = a2
    while true do
        yield b1
        let b3 = b1 + b2
        b1 <- b2
        b2 <- b3
}

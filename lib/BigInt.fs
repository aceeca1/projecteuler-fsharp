module BigInt
let sqrt(n: bigint) =
    let a = System.Numerics.BigInteger.Log(n, 2.0)
    let mutable b = 1I <<< (a * 0.5 |> round |> int)
    let mutable break1 = false
    while not break1 do
        let b1 = (b + n / b) >>> 1
        if abs(b - b1) <= 3I then break1 <- true
        else b <- b1
    while b * b <= n do b <- b + 1I
    while b * b > n do b <- b - 1I
    b

module Cycle
let find(n, f) =
    let rec g(idx1, n1, idx2, n2) =
        if n1 = n2 then idx2 - idx1
        elif idx2 > idx1 + idx1 then g(idx2, n2, idx2 + 1, f(n2))
        else g(idx1, n1, idx2 + 1, f(n2))
    g(0, n, 1, f(n))

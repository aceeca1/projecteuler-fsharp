module Hash
let h128to64(u1, u2) =
    let kMul = 0x9ddfea08eb382d69L
    let a1 = (u1 ^^^ u2) * kMul
    let a2 = a1 ^^^ (a1 >>> 47)
    let b1 = (a2 ^^^ u1) * kMul
    let b2 = b1 ^^^ (b1 >>> 47)
    b2 * kMul
let h64(u) = h128to64(u, 0L)

module Polygonal
let num(k, n) = (n * (n - 1L) >>> 1) * (int64 k - 2L) + n
let is(k, n) =
    match k with
    | 3 -> n = num(k, n + n |> float |> sqrt |> int64)
    | 4 -> n = num(k, n |> float |> sqrt |> int64)
    | _ -> n = num(k, (float(n + n) / float(k - 2) |> sqrt |> int64) + 1L)

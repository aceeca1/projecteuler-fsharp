(* We have n(n - 1) = 2k(k - 1),
    i.e.  (2n - 1)^2 - 2(2k - 1)^2 = -1
    which is a Pell equation. Use wolframalpha.com .*)
let se = seq {
    let sqrt2 = sqrt 2.0
    let mutable a = (2.0 - sqrt2) * 0.125
    let mutable b = (sqrt2 - 1.0) * 0.25
    let c = 3.0 + 2.0 * sqrt2
    while true do
        a <- a * c
        b <- b * c
        yield a + 0.5 |> round |> int64, b + 0.5 |> round |> int64
}
let k, _ = se |> Seq.find(fun (k, n) -> n > 1000000000000L)
k |> printfn "%d"

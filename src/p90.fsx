let cubeD = ResizeArray<int>()
let m69 = (1 <<< 6) + (1 <<< 9)
let rec put(k, c, bs) =
    if k = 6 then
        let bs1 = if bs &&& m69 = 0 then bs else bs ||| m69
        bs1 |> cubeD.Add |> ignore
    elif c < 10 then
        put(k + 1, c + 1, bs ||| (1 <<< c))
        put(k, c + 1, bs)
put(0, 0, 0)
let mutable ans = 0
for i in cubeD do
    for j in cubeD do
        if i <= j && seq {1 .. 9} |> Seq.forall(fun k ->
            let k2 = k * k
            let p1 = k2 / 10
            let p2 = k2 % 10
            let f(a1, a2) =
                ((a1 >>> p1) &&& 1 <> 0) && ((a2 >>> p2) &&& 1 <> 0)
            f(i, j) || f(j, i))
        then
            ans <- ans + 1
ans |> printfn "%d"

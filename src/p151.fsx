type Dist = System.Collections.Generic.Dictionary<int[], float>
let eqInts = {
    new System.Collections.Generic.IEqualityComparer<int[]> with
        member u.Equals(v1, v2) = v1 = v2
        member u.GetHashCode(v) = hash v }
let mutable a = Dist(eqInts)
a.Add([|1; 1; 1; 1|], 1.0)
let next(a: Dist) =
    let ret = Dist(eqInts)
    for kv in a do
        let k = kv.Key
        let v = kv.Value
        let d = k |> Array.sum
        for i = 0 to 3 do
            if k.[i] <> 0 then
                let k1 = k.Clone() :?> int[]
                k1.[i] <- k1.[i] - 1
                for j = i + 1 to 3 do k1.[j] <- k1.[j] + 1
                let v1 = v / float d * float k.[i]
                if ret.ContainsKey(k1) then ret.[k1] <- ret.[k1] + v1
                else ret.Add(k1, v1)
    ret
let prob1(a: Dist) =
    let mutable ret = 0.0
    for kv in a do if kv.Key |> Array.sum = 1 then ret <- ret + kv.Value
    ret
let mutable s = 0.0
for i = 2 to 14 do
    a <- next(a)
    s <- s + prob1(a)
s |> printfn "%.6f"

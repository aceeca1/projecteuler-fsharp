let file = new System.IO.StreamReader("../data/input99.txt")
let mutable ans, iAns = 0.0, -1
for i = 1 to 1000 do
    let line = file.ReadLine().Split(',') |> Array.map(float)
    let bas = line.[0]
    let exp = line.[1]
    let v = log(bas) * exp
    if v > ans then
        ans <- v
        iAns <- i
iAns |> printfn "%d"

let mutable numer1, denom1 = 0, 1
let mutable numer2, denom2 = 1, 1
let mutable break1 = false
while not break1 do
    let numerM = numer1 + numer2
    let denomM = denom1 + denom2
    if denomM > 1000000 then break1 <- true
    elif numerM * 7 >= denomM * 3 then
        numer2 <- numerM
        denom2 <- denomM
    else
        numer1 <- numerM
        denom1 <- denomM
numer1 |> printfn "%d"

open System.Linq
let n = 10000000
let a = Array.zeroCreate<int>(n)
a.[1] <- 1
a.[89] <- 89
let next(m: int) = (string m).Sum(fun i -> let k = int i - int '0' in k * k)
let rec calc(m) =
    if a.[m] <> 0 then a.[m] else
        let ret = calc(next(m))
        a.[m] <- ret
        ret
let mutable ans = 0
for i = 1 to n - 1 do
    if calc(i) = 89 then ans <- ans + 1
ans |> printfn "%d"

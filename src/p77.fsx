#load "../lib/Prime.fs"
let mutable ans = 0
try
    let mutable ub = 1000
    while true do
        let a = Prime.sieve(ub)
        let b = Array.zeroCreate<int>(ub + 1)
        b.[0] <- 1
        let mutable i = 2
        while i <= ub do
            for j = i to ub do b.[j] <- b.[j] + b.[j - i]
            i <- a.[i]
        for i = 1 to ub do
            if b.[i] >= 5000 then
                ans <- i
                failwith "found"
        ub <- ub + ub
with _ -> ans |> printfn "%d"

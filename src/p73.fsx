#load "../lib/Divisor.fs"
let mutable ans = 0
for i = 2 to 12000 do
    for j = i / 3 + 1 to (i - 1) / 2 do
        if Divisor.gcd(i, j) = 1 then ans <- ans + 1
ans |> printfn "%d"

#load "../lib/Prime.fs"
let isPan(n: int) =
    let mutable a = string n
    let mutable s = 0
    for i in a do s <- s ||| (1 <<< (int i - int '0'))
    s = (((1 <<< a.Length) - 1) <<< 1)
let a = Prime.sieve(10000000)
let mutable ans = 0
let mutable i = 2
while i < 10000000 do
    if isPan(i) then ans <- i
    i <- a.[i]
ans |> printfn "%d"

#load "../lib/Polygonal.fs"
open System.Linq
let file = new System.IO.StreamReader("../data/input42.txt")
let strip(s: string) = s.[1 .. s.Length - 2]
let a = file.ReadLine().Split(',') |> Array.map(strip)
let mutable ans = 0
for ai in a do
    let s = ai.Sum(fun i -> int i - int 'A' + 1)
    if Polygonal.is(3, int64 s) then ans <- ans + 1
ans |> printfn "%d"

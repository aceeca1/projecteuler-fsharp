#load "../lib/Cycle.fs"
let hasOddPeriod(n) =
    let k = n |> float |> sqrt |> int
    k * k <> n &&
    let c = Cycle.find([|0; 0; 1|], fun a ->
        let a2 = (n - a.[1] * a.[1]) / a.[2]
        let a0 = (k - a.[1]) / a2
        let a1 = -a0 * a2 - a.[1]
        [|a0; a1; a2|])
    c &&& 1 <> 0
let mutable ans = 0
for i = 1 to 10000 do if hasOddPeriod(i) then ans <- ans + 1
ans |> printfn "%d"

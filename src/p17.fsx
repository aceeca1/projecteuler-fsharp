let pronC(n) =
    match n with
    | 1 -> "one"       | 2 -> "two"        | 3 -> "three"
    | 4 -> "four"      | 5 -> "five"       | 6 -> "six"
    | 7 -> "seven"     | 8 -> "eight"      | 9 -> "nine"
    | 10 -> "ten"      | 11 -> "eleven"    | 12 -> "twelve"
    | 13 -> "thirteen" | 14 -> "fourteen"  | 15 -> "fifteen"
    | 16 -> "sixteen"  | 17 -> "seventeen" | 18 -> "eighteen"
    | 19 -> "nineteen" | 20 -> "twenty"    | 30 -> "thirty"
    | 40 -> "forty"    | 50 -> "fifty"     | 60 -> "sixty"
    | 70 -> "seventy"  | 80 -> "eighty"    | 90 -> "ninety"
    | 1000 -> "oneThousand"                | _ -> null
let rec pron(n) =
    match pronC(n) with
    | null when n < 100 -> pronC(n / 10 * 10).Length + pronC(n % 10).Length
    | null when n % 100 = 0 -> pronC(n / 100).Length + "hundred".Length
    | null -> pron(n / 100 * 100) + "and".Length + pron(n % 100)
    | s -> s.Length
let mutable ans = 0
for i = 1 to 1000 do ans <- ans + pron(i)
ans |> printfn "%d"

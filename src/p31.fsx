let n = 200
let a = Array.zeroCreate<int>(n + 1)
a.[0] <- 1
for i in [|1; 2; 5; 10; 20; 50; 100; 200|] do
    for j = i to n do
        a.[j] <- a.[j] + a.[j - i]
a.[n] |> printfn "%d"

#load "../lib/Divisor.fs"
(* Enumerate one point and count different positions of the other. *)
let mutable ans = 0
for i = 1 to 50 do
    for j = 1 to 50 do
        ans <- ans + min(50 - i)(j * j / i) / (j / Divisor.gcd(i, j))
ans + ans + 3 * 50 * 50 |> printfn "%d"

#load "../lib/Prime.fs"
let mutable all = 1
let mutable pri = 0
let mutable n = 1
let mutable d = 2
while n = 1 || float pri / float all >= 0.1 do
    for i = 1 to 4 do
        n <- n + d
        all <- all + 1
        if Prime.isL(n) then pri <- pri + 1
    d <- d + 2
d - 1 |> printfn "%d"

#load "../lib/Prime.fs"
let n = 1000000
let a = Prime.sieve(n)
let mutable ans, iAns = 0, -1
let mutable i = 2
while i < n && i * ans < n do
    let mutable s = i
    let mutable j = a.[i]
    while j < n && s < n do
        s <- s + j
        if s < n && a.[s] > s then
            let len = j - i + 1
            if len > ans then
                ans <- len
                iAns <- s
        j <- a.[j]
    i <- a.[i]
iAns |> printfn "%d"

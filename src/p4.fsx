let isPalindrome(n) =
    let s = string(n)
    let a = s.ToCharArray()
    System.Array.Reverse(a)
    a = s.ToCharArray()
let mutable ans = 0
for i1 = 100 to 999 do
    for i2 = 100 to 999 do
        let n = i1 * i2
        if isPalindrome n then ans <- max(ans)(n)
ans |> printfn "%d"

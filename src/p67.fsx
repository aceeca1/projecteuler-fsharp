let file = new System.IO.StreamReader("../data/input67.txt")
let n = 100
let a = Array.init(n)(fun i ->
    file.ReadLine().Split() |> Array.map(int))
let rec f(m, ans: int[]) =
    if m = -1 then ans.[0] else
        f(m - 1, Array.init(m + 1)(fun i ->
            max(ans.[i])(ans.[i + 1]) + a.[m].[i]))
f(n - 2, a.[n - 1]) |> printfn "%d"

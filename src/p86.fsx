(* Assume a <= b <= c <= m, and p = a + b. We need p^2 + c^2 = q^2. *)
let n = 1000000
let rec f(m, s) =
    if s > n then m else
        let mutable u = 0
        let m1 = m + 1
        let c = m1
        let c2 = c * c
        for p = 2 to c + c do
            let q2 = p * p + c * c
            let q = q2 |> float |> sqrt |> int
            if q * q = q2 then
                let lb = max(1)(p - c)
                let ub = p >>> 1
                u <- u + (ub - lb + 1)
        f(m + 1, s + u)
f(0, 0) |> printfn "%d"

#load "../lib/Prime.fs"
open System.Linq
let mutable ans = 0
try
    let mutable i = 2
    while true do
        let s = string i
        for j in "012" do
            if s.IndexOf(j) <> -1 then
                let a = ResizeArray<string>()
                let pat = System.Text.StringBuilder()
                let rec f(m) =
                    if m = s.Length then a.Add(pat.ToString()) |> ignore else
                        if s.[m] = j then
                            pat.Append('*') |> ignore
                            f(m + 1)
                            pat.Remove(pat.Length - 1, 1) |> ignore
                        pat.Append(s.[m]) |> ignore
                        f(m + 1)
                        pat.Remove(pat.Length - 1, 1) |> ignore
                f(0)
                a.RemoveAt(a.Count - 1)
                for ai in a do
                    let mutable b = 0
                    for c = int j + 1 to int '9' do
                        let aic = ai.ToCharArray()
                        for k = 0 to aic.Length - 1 do
                            if aic.[k] = '*' then aic.[k] <- char c
                        let v = aic.Aggregate(0, fun s i ->
                            s * 10 + (int i - int '0'))
                        if Prime.is(v) then b <- b + 1
                    if b >= 7 then
                        ans <- i
                        failwith "found"
        i <- Prime.next(i)
with _ -> ans |> printfn "%d"

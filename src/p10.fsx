#load "../lib/Prime.fs"
let n = 2000000
let a = Prime.sieve(n)
let mutable ans = 0L
let mutable i = 2
while i < n do
    ans <- ans + int64 i
    i <- a.[i]
ans |> printfn "%d"

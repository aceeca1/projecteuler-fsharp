#load "../lib/Cycle.fs"
let mutable ans, iAns = 0, -1
for i = 1 to 1000 do
    let c = Cycle.find(1, fun u -> u * 10 % i)
    if c > ans then
        ans <- c
        iAns <- i
iAns |> printfn "%d"

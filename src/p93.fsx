let arithExpFrom1(arr: seq<int>) =
    let v = System.Collections.Generic.HashSet<int>()
    let a = arr |> Seq.map(float) |> ResizeArray
    let aWithout(m, func) =
        let am = a.[m]
        a.[m] <- a.[a.Count - 1]
        a.RemoveAt(a.Count - 1)
        func(am)
        if m = a.Count then a.Add(am) |> ignore else
            a.Add(a.[m]) |> ignore
            a.[m] <- am
    let aWith(num, func) =
        a.Add(num) |> ignore
        func()
        a.RemoveAt(a.Count - 1)
    let rec visit() =
        printfn "%A" a
        if a.Count = 1 then
            let ar = a.[0] |> round
            if abs(ar - a.[0]) < 1e-9 then
                let aI = int ar
                if aI >= 1 then v.Add(aI) |> ignore
        else
            for i = 0 to a.Count - 1 do aWithout(i, fun ai ->
                for j = i to a.Count - 1 do aWithout(j, fun aj ->
                    aWith(ai + aj, visit)
                    aWith(ai - aj, visit)
                    aWith(aj - ai, visit)
                    aWith(ai * aj, visit)
                    aWith(ai / aj, visit)
                    aWith(aj / ai, visit)))
    visit()
    let rec f(m) = if v.Contains(m) |> not then m - 1 else f(m + 1)
    f(1)
let dComb(u: int[], k, f) =
    let a = ResizeArray<int>()
    let rec g(m, k) =
        if k = 0 then f(a)
        elif m < u.Length then
            a.Add(u.[m]) |> ignore
            g(m + 1, k - 1)
            a.RemoveAt(a.Count - 1)
            g(m + 1, k)
    g(0, k)
let mutable ans = 0
let mutable iAns = null: int[]
dComb([|1 .. 9|], 4, fun u ->
    let k = arithExpFrom1(u)
    if k > ans then
        ans <- k
        iAns <- u.ToArray())
iAns |> System.String.Concat |> printfn "%s"

let mutable ans = 0
let rec f(m, c) =
    if m <= 100 then
        for i in c do if i > 1.0e6 then ans <- ans + 1
        f(m + 1, Array.init(m + 2)(fun i ->
            if i = 0 || i = m + 1 then 1.0 else c.[i - 1] + c.[i]))
f(1, [|1.0; 1.0|])
ans |> printfn "%d"

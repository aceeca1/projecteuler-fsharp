#load "../lib/Phi.fs"
#load "../lib/Prime.fs"
let n = 1000000
let a = Prime.sieve(n)
let phi = Phi.compute(a)
(phi |> Array.sumBy(fun i -> int64 i)) - 1L |> printfn "%d"

#load "../lib/Divisor.fs"
(* All Pythagorean triples can be generated from
    (k(p^2 - q^2))^2 + (k(2pq)^2)^2 = (k(p^2 + q^2))^2 .
The sum is 2p(p + q) = u. *)
let n = 1500000
let a = Array.zeroCreate<int>(n + 1)
let mutable p = 1
while p * (p + 1) <<< 1 <= n do
    for q in p - 1 .. -2 .. 1 do
        let u = p * (p + q) <<< 1
        if u <= n && Divisor.gcd(p, q) = 1 then
            for i in u .. u .. n do a.[i] <- a.[i] + 1
    p <- p + 1
let mutable ans = 0
for i = 0 to n do
    if a.[i] = 1 then ans <- ans + 1
ans |> printfn "%d"

let cR = "23456789TJQKA"
let cRank = Array.zeroCreate<int>(256)
for i = 0 to cR.Length - 1 do cRank.[int cR.[i]] <- i
let pokerR(c: string[]) =
    let flush = c |> Seq.forall(fun s -> s.[1] = c.[0].[1])
    let a = Array.zeroCreate<int>(13)
    for i = 0 to 4 do
        let k = cRank.[int c.[i].[0]]
        a.[k] <- a.[k] + 1
    let mutable k = 0
    while a.[k] = 0 do k <- k + 1
    let straight = seq {k .. k + 4} |> Seq.forall(fun i -> a.[i] = 1)
    let straight0 =
        a.[12] = 1 && a.[0] = 1 && a.[1] = 1 &&
        a.[2] = 1  && a.[3] = 1
    if flush then
        if straight then
            let sb = System.Text.StringBuilder("9")
            sb.Append(char(int 'E' + k)).ToString()
        elif straight0 then "9D" else "6"
    elif straight then "5"
    elif straight0 then "5D" else
        let b = Array.init(5)(fun i -> ResizeArray<int>())
        for i = 12 downto 0 do if a.[i] <> 0 then b.[a.[i]].Add(i) |> ignore
        let mutable ret = System.Text.StringBuilder()
        if b.[4].Count = 1 then ret.Append('8') |> ignore
        elif b.[3].Count = 1 && b.[2].Count = 1 then ret.Append('7') |> ignore
        elif b.[3].Count = 1 then ret.Append('4') |> ignore
        elif b.[2].Count = 2 then ret.Append('3') |> ignore
        elif b.[2].Count = 1 then ret.Append('2') |> ignore
        else ret.Append('1') |> ignore
        for i = 4 downto 1 do
            for j in b.[i] do ret.Append(char(int 'A' + j)) |> ignore
        ret.ToString()
let file = new System.IO.StreamReader("../data/input54.txt")
let mutable ans = 0
for i = 1 to 1000 do
    let rs = file.ReadLine().Split()
    let p1 = pokerR(rs.[0 .. 4])
    let p2 = pokerR(rs.[5 .. 9])
    if p1 > p2 then ans <- ans + 1
ans |> printfn "%d"

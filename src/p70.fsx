#load "../lib/Hash.fs"
#load "../lib/Phi.fs"
#load "../lib/Prime.fs"
let n = 10000000
let a = Prime.sieve(n)
let phi = Phi.compute(a)
let mutable ans, iAns = System.Double.PositiveInfinity, -1
for i = 2 to n - 1 do
    let mutable h1 = 0L
    for j in string i do
        h1 <- h1 + Hash.h64(int64 j)
    let mutable h2 = 0L
    for j in string phi.[i] do
        h2 <- h2 + Hash.h64(int64 j)
    if h1 = h2 then
        let v = float i / float phi.[i]
        if v < ans then
            ans <- v
            iAns <- i
iAns |> printfn "%d"

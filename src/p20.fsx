let mutable a = 1I
for i = 2 to 100 do a <- a * bigint i
let mutable ans = 0
for i in string a do
    ans <- ans + (int i - int '0')
ans |> printfn "%d"

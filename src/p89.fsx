let roman1 = "IVXLCDM"
let roman2 = [|1; 5; 10; 50; 100; 500; 1000|]
let romanL = Array.zeroCreate<int>(256)
for i = 0 to roman1.Length - 1 do romanL.[int roman1.[i]] <- roman2.[i]
let romanNum(ro: string) =
    let rec f(m, ans, pR) =
        if m = ro.Length then ans + pR else
            let cR = romanL.[int ro.[m]]
            if pR = 0 then f(m + 1, ans, cR)
            elif pR < cR then f(m + 1, ans + cR - pR, 0)
            else f(m + 1, ans + pR, cR)
    f(0, 0, 0)
let romanP = [|
    1000, "M"; 900, "CM"; 500, "D"; 400, "CD"
    100,  "C"; 90,  "XC"; 50,  "L"; 40,  "XL"
    10,   "X"; 9,   "IX"; 5,   "V"; 4,   "IV"; 1, "I"|]
let numRoman(n) =
    let ret = System.Text.StringBuilder()
    let mutable k = n
    for num, ro in romanP do
        while k >= num do
            k <- k - num
            ret.Append(ro) |> ignore
    ret.ToString()
let file = new System.IO.StreamReader("../data/input89.txt")
Seq.init(1000)(fun i ->
    let s = file.ReadLine()
    let s1 = s |> romanNum |> numRoman
    s.Length - s1.Length) |> Seq.sum |> printfn "%d"

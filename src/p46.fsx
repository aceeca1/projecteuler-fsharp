#load "../lib/Prime.fs"
type TwoSSeq(x1) as u =
    let mutable x2 = -1
    let mutable v = 0
    do u.MoveNext()
    member u.X1 = x1
    member u.V = v
    member u.MoveNext() =
        x2 <- x2 + 1
        v <- x1 + (x2 * x2 <<< 1)
    interface System.IComparable<TwoSSeq> with
        member u1.CompareTo(u2) = compare (u1.V, u1.X1) (u2.V, u2.X1)
type PrimeTwoSSeq() =
    let mutable a = 2
    let qu = System.Collections.Generic.SortedSet<TwoSSeq>()
    do qu.Add(TwoSSeq(2)) |> ignore
    member u.V = qu.Min.V
    member u.MoveNext() =
        let quMin = qu.Min
        qu.Remove(quMin) |> ignore
        if quMin.X1 = a then
            a <- Prime.next(a)
            qu.Add(TwoSSeq(a)) |> ignore
        quMin.MoveNext()
        qu.Add(quMin) |> ignore
let mutable ans = 0
try
    let a = PrimeTwoSSeq()
    let mutable i = 3
    while true do
        while a.V < i do a.MoveNext()
        if a.V <> i then
            ans <- i
            failwith "found"
        i <- i + 2
with _ -> ans |> printfn "%d"

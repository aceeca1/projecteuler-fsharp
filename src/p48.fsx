let rec pow(a, b, ans) =
    if b = 0 then ans else pow(a, b - 1, ans * a % 10000000000L)
let mutable ans = 0L
for i = 1 to 1000 do
    ans <- ans + pow(int64 i, i, 1L)
ans % 10000000000L |> printfn "%d"

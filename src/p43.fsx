open System.Linq
let mutable ans = 0L
let ms = [|2; 3; 5; 7; 11; 13; 17|]
let rec put(a: int[], m) =
    if m = -1 then
        ans <- ans + a.Aggregate(0L, fun s i -> s * 10L + int64 i)
    elif m >= 7 ||
        (a.[m + 1] * 100 + a.[m + 2] * 10 + a.[m + 3]) % ms.[m] = 0
    then
        let am = a.[m]
        for i = 0 to m do
            a.[m] <- a.[i]
            a.[i] <- am
            put(a, m - 1)
            a.[i] <- a.[m]
put([|0 .. 9|], 9)
ans |> printfn "%d"

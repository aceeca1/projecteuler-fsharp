#load "../lib/Hash.fs"
let file = new System.IO.StreamReader("../data/input42.txt")
let strip(s: string) = s.[1 .. s.Length - 2]
let a = System.Collections.Generic.Dictionary<int64, ResizeArray<string>>()
for i in file.ReadLine().Split(',') |> Array.map(strip) do
    let mutable h = 0L
    for j in i do h <- h + Hash.h64(int64 j)
    if a.ContainsKey(h) |> not then a.Add(h, ResizeArray<string>()) |> ignore
    a.[h].Add(i) |> ignore
let tenY = [|
    1; 10; 100; 1000; 10000; 100000; 1000000;
    10000000; 100000000; 1000000000|]
let mutable ans = 0
let trans(s1: string, s2: string) =
    let ret = Array.zeroCreate<char>(256)
    for i = 0 to s1.Length - 1 do
        ret.[int s1.[i]] <- s2.[i]
        ret.[int s2.[i]] <- s1.[i]
    for i = 0 to s1.Length - 1 do
        if ret.[int s1.[i]] <> s2.[i] then failwith "no trans"
        if ret.[int s2.[i]] <> s1.[i] then failwith "no trans"
    ret
for ai in a do
    let v = ai.Value
    for i1 = 0 to v.Count - 1 do
        for i2 = i1 + 1 to v.Count - 1 do
            let n = v.[i1].Length
            let lb = tenY.[n - 1] |> float |> sqrt |> round |> int
            let ub = tenY.[n] - 1 |> float |> sqrt |> round |> int
            for k1 = lb to ub do
                let s1 = string(k1 * k1)
                if s1.Length = n then
                    try
                        let tr = trans(v.[i1], s1)
                        let s2 = v.[i2] |> String.map(fun i -> tr.[int i])
                        if s2.[0] = '0' then failwith "leading zero"
                        let num2 = s2 |> int
                        let k2 = num2 |> float |> sqrt |> int
                        if k2 * k2 = num2 then
                            for k in [|k1; k2|] do
                                if k > ans then ans <- k
                    with _ -> ()
ans * ans |> printfn "%d"

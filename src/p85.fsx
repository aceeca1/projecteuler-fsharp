let n = 2000000
let mutable ans, iAns = System.Int32.MaxValue, -1
let mutable a, b = 1, 2000
while b <> 0 do
    let aS = a * (a + 1) >>> 1
    let bS = b * (b + 1) >>> 1
    let c = aS * bS
    let d = abs(c - n)
    if d < ans then
        ans <- d
        iAns <- a * b
    if c < n then a <- a + 1
    else b <- b - 1
iAns |> printfn "%d"

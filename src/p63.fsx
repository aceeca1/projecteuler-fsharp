let mutable ans = 0
for i = 1 to 9 do
    let rec f(m, a) =
        let s = string a
        if s.Length = m then ans <- ans + 1
        if s.Length >= m then f(m + 1, a * bigint i)
    f(1, bigint i)
ans |> printfn "%d"

let file = new System.IO.StreamReader("../data/input59.txt")
let a = Array2D.zeroCreate<int>(3)(256)
let mutable c = 0
for i in file.ReadLine().Split(',') do
    a.[c, int i] <- a.[c, int i] + 1
    c <- if c = 2 then 0 else c + 1
let key = Array.init(3)(fun i ->
    let m = (seq {0 .. 255}) |> Seq.maxBy(fun j -> a.[i, j])
    m ^^^ int ' ')
let mutable ans = 0
for i = 0 to 2 do
    for j = 0 to 255 do
        ans <- ans + a.[i, j] * (j ^^^ key.[i])
ans |> printfn "%d"

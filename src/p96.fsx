#load "../lib/Binary.fs"
let e = Array.init(81)(fun i -> ResizeArray<int>())
let no(xu, xv, yu, yv) = (xu * 3 + xv) * 9 + (yu * 3 + yv)
for x1u = 0 to 2 do
  for x1v = 0 to 2 do
    for y1u = 0 to 2 do
      for y1v = 0 to 2 do
        for x2u = 0 to 2 do
          for x2v = 0 to 2 do
            for y2u = 0 to 2 do
              for y2v = 0 to 2 do
                let b1() = x1u = x2u && x1v = x2v
                let b2() = y1u = y2u && y1v = y2v
                let b3() = x1u = x2u && y1u = y2u
                if b1() || b2() || b3() then
                    let no1 = no(x1u, x1v, y1u, y1v)
                    let no2 = no(x2u, x2v, y2u, y2v)
                    if no1 <> no2 then
                        e.[no1].Add(no2) |> ignore
type Sudoku(v: int[]) =
    [<DefaultValue>] val mutable Sol: Sudoku
    let iStart() =
        try
            let mutable ans, iAns = System.Int32.MaxValue, -1
            for i = 0 to 80 do
                match Binary.count14(v.[i]) with
                | 0 -> failwith "no solution"
                | 1 -> ()
                | c -> if c < ans then ans <- c; iAns <- i
            iAns
        with _ -> -2
    member u.Update(pos) =
        for i in e.[pos] do
            let b0 = v.[i] &&& (v.[i] - 1) = 0
            v.[i] <- v.[i] &&& ~~~v.[pos]
            if v.[i] = 0 then failwith "no solution"
            let b1 = v.[i] &&& (v.[i] - 1) = 0
            if not b0 && b1 then u.Update(i)
    member u.TryUpdate(pos) = try u.Update(pos); true with _ -> false
    static member Read(read: unit -> string) =
        let v = Array.zeroCreate<int>(81)
        read() |> ignore
        let mutable k = 0
        for i = 0 to 8 do
            let s = read()
            for j = 0 to 8 do
                v.[k] <-
                    match int s.[j] - int '0' with
                    | 0 -> (1 <<< 10) - 2
                    | p -> 1 <<< p
                k <- k + 1
        let ret = Sudoku(v)
        for i = 0 to 80 do
            if v.[i] &&& (v.[i] - 1) = 0 then ret.Update(i)
        ret
    override u.ToString() =
        let sb = System.Text.StringBuilder()
        let mutable k = 0
        for i = 0 to 8 do
            for j = 0 to 8 do
                sb.Append(
                    match Binary.count14(v.[k]) with
                    | 1 -> int '0' + Binary.which(v.[k]) |> char
                    | 0 -> '#'
                    | _ -> '0') |> ignore
                k <- k + 1
            sb.Append('\n') |> ignore
        sb.ToString()
    member u.Childs(iS) = seq {
        let mutable s = v.[iS]
        while s <> 0 do
            let i = s &&& -s
            let v1 = v.Clone() :?> int[]
            v1.[iS] <- i
            let ch = Sudoku(v1)
            if ch.TryUpdate(iS) then yield ch
            s <- s - i
    }
    member u.Solve(ancestor: Sudoku) =
        match iStart() with
        | -1 ->
            ancestor.Sol <- u
            failwith "found solution"
        | -2 -> ()
        | iS -> for i in u.Childs(iS) do i.Solve(ancestor)
    member u.TrySolve() = try u.Solve(u) with _ -> ()
let file = new System.IO.StreamReader("../data/input96.txt")
let mutable ans = 0
for i = 1 to 50 do
    let sudoku = Sudoku.Read(file.ReadLine)
    sudoku.TrySolve()
    ans <- ans + int(sudoku.Sol.ToString().[.. 2])
ans |> printfn "%d"

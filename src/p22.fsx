open System.Linq
let file = new System.IO.StreamReader("../data/input22.txt")
let strip(s: string) = s.[1 .. s.Length - 2]
let a = file.ReadLine().Split(',') |> Array.map(strip)
System.Array.Sort(a)
let mutable ans = 0
for i = 0 to a.Length - 1 do
    ans <- ans + (i + 1) * a.[i].Sum(fun i -> int i - int 'A' + 1)
ans |> printfn "%d"

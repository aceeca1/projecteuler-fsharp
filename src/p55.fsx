let isLychrel(n: bigint) =
    (seq {
        let mutable a = n
        let mutable s = string a
        let mutable sR = s.ToCharArray()
        System.Array.Reverse(sR)
        for i = 1 to 50 do
            a <- a + bigint.Parse(System.String(sR))
            s <- string a
            sR <- s.ToCharArray()
            System.Array.Reverse(sR)
            yield s.ToCharArray() <> sR
    }) |> Seq.forall(id)
let mutable ans = 0
for i = 1 to 9999 do if isLychrel(bigint i) then ans <- ans + 1
ans |> printfn "%d"

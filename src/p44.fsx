#load "../lib/Divisor.fs"
#load "../lib/Polygonal.fs"
(* {a, b, a + b, a - b} are pentagonal. Let c = a - b, and they becomes
{b, c, c + b, c + 2b}. Let c + b = 3q(q - 1) / 2, b = 3s(s - 1) / 2.
We have (3q + 3s + 1)(q - s) = 2c. *)
let mutable ans = 0
try
    let mutable i = 2
    while true do
        let mutable c = Polygonal.num(5, int64 i) |> int
        let c2 = c + c
        for k1 in Divisor.ofNum(c2) do
            let k2 = k1 + 1
            if k2 % 3 = 0 then
                let k3 = k2 / 3
                let k4 = c2 / k1
                if k3 > k4 then
                    let k5 = k3 + k4
                    if k5 &&& 1 = 0 then
                        let q = k5 >>> 1
                        let s = k3 - q
                        let a = Polygonal.num(5, int64 q)
                        let b = Polygonal.num(5, int64 s)
                        if Polygonal.is(5, a + b) then
                            ans <- c
                            failwith "found"
        i <- i + 1
with _ -> ans |> printfn "%d"

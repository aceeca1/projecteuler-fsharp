let n = 12000
let mps = Array.create(n + 1)(System.Int32.MaxValue)
let rec f(pSum, pProd, cSize, c) =
    let cSum = pSum + c
    let cProd = pProd * c
    let fSize = cSize + (cProd - cSum)
    if fSize <= n && not(cSize = 1 && c * c > n) then
        if cProd < mps.[fSize] then mps.[fSize] <- cProd
        f(cSum, cProd, cSize + 1, c)
        f(pSum, pProd, cSize, c + 1)
f(0, 1, 1, 2)
let a = System.Collections.Generic.HashSet<int>()
for i = 2 to n do a.Add(mps.[i]) |> ignore
a |> Seq.sum |> printfn "%d"

#load "../lib/BigInt.fs"
let goo2 = System.Numerics.BigInteger.Pow(10I, 200)
let mutable ans = 0
for i = 1 to 100 do
    let b = i |> float |> sqrt |> int
    if b * b <> i then
        let a = goo2 * bigint i
        for j in BigInt.sqrt(a).ToString().[.. 99] do
            ans <- ans + (int j - int '0')
ans |> printfn("%d")

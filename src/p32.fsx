let isPan(a: int[]) =
    let mutable s = 0
    for i in a do
        for j in string i do
            s <- s ||| (1 <<< (int j - int '0'))
    s = 0x3fe
let a = System.Collections.Generic.HashSet<int>()
let rec f(n1, n2) =
    for i1 = n1 to n1 * 10 - 1 do
        let mutable i2 = n2
        while i2 < n2 * 10 do
            let i3 = i1 * i2
            if i3 >= 10000 then i2 <- n2 * 10 else
                if isPan([|i1; i2; i3|]) then a.Add(i3) |> ignore
                i2 <- i2 + 1
f(10, 100)
f(1, 1000)
a |> Seq.sum |> printfn "%d"

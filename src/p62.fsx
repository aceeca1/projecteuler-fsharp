#load "../lib/Hash.fs"
let mutable k = 0
let a = System.Collections.Generic.Dictionary<int64, ResizeArray<int64>>()
let mutable ans = System.Int64.MaxValue
let mutable i = 1
while ans = System.Int64.MaxValue do
    let m = int64 i * int64 i * int64 i
    let mutable h = 0L
    let s = string m
    for i in s do h <- h + Hash.h64(int64 i)
    if s.Length = k then
        if a.ContainsKey(h) |> not then
            a.Add(h, ResizeArray<int64>()) |> ignore
        a.[h].Add(m) |> ignore
    else
        for i in a do
            let v = i.Value
            if v.Count >= 5 && v.[0] < ans then ans <- v.[0]
        a.Clear()
        k <- s.Length
    i <- i + 1
ans |> printfn "%d"

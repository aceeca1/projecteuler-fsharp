#load "../lib/Prime.fs"
let n = 50000000
let a = Prime.sieve(n |> float |> sqrt |> int)
let cPrime(f) =
    let ret = ResizeArray<int>()
    let mutable i = 2
    while i > 0 do
        let u = f(i)
        if u > n then i <- -1 else
            ret.Add(u) |> ignore
            i <- a.[i]
    ret
let a2 = cPrime(fun i -> i * i)
let a3 = cPrime(fun i -> i * i * i)
let a4 = cPrime(fun i -> (i * i) * (i * i))
let ans = System.Collections.Generic.HashSet<int>()
for i2 in a2 do
    for i3 in a3 do
        let mutable k = 0
        while k < a4.Count do
            let i4 = a4.[k]
            let b = i2 + i3 + i4
            if b >= n then k <- a4.Count else
                ans.Add(b) |> ignore
                k <- k + 1
ans.Count |> printfn "%d"

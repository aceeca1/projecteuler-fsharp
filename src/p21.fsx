#load "../lib/Divisor.fs"
let isAmicable(n) =
    let m = Divisor.sum(n)
    n <> m && n = Divisor.sum(m)
let mutable ans = 0
for i = 1 to 10000 do if isAmicable(i) then ans <- ans + i
ans |> printfn "%d"

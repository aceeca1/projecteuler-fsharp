let a = Array.init(10)(fun i -> i * i * i * i * i)
let isSpecial(n) =
    let s = string n
    let mutable ans = 0
    for i in s do ans <- ans + a.[int i - int '0']
    ans = n
let mutable ans = 0
for i = 2 to 6 * a.[9] do
    if isSpecial(i) then ans <- ans + i
ans |> printfn "%d"

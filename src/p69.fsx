#load "../lib/Phi.fs"
#load "../lib/Prime.fs"
let n = 1000000
let a = Prime.sieve(n)
let phi = Phi.compute(a)
let mutable ans, iAns = 0.0, -1
for i = 1 to n do
    let v = float i / float phi.[i]
    if v > ans then
        ans <- v
        iAns <- i
iAns |> printfn "%d"

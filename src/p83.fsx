#load "../lib/Graph.fs"
let file = new System.IO.StreamReader("../data/input81.txt")
let n = 80
let a = Array.init(n)(fun i -> file.ReadLine().Split(',') |> Array.map(int))
let b = Graph.Builder<int, int * int>()
for i = 0 to n - 1 do
    for j = 0 to n - 1 do
        if j <> 0 then
            b.AddEdge((i, j - 1), (i, j), a.[i].[j])
            b.AddEdge((i, j), (i, j - 1), a.[i].[j - 1])
        if i <> 0 then
            b.AddEdge((i - 1, j), (i, j), a.[i].[j])
            b.AddEdge((i, j), (i - 1, j), a.[i - 1].[j])
let noS = b.AddVertex(0, 0)
let noT = b.AddVertex(n - 1, n - 1)
Graph.shortPath(b.G, noS).[noT] + a.[0].[0] |> printfn "%d"

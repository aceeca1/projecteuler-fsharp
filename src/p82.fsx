#load "../lib/Graph.fs"
let file = new System.IO.StreamReader("../data/input81.txt")
let n = 80
let a = Array.init(n)(fun i -> file.ReadLine().Split(',') |> Array.map(int))
let b = Graph.Builder<int, int * int>()
let noS = b.AddVertexR()
let noT = b.AddVertexR()
for i = 0 to n - 1 do
    for j = 0 to n - 1 do
        if j <> 0 then
            b.AddEdge((i, j - 1), (i, j), a.[i].[j])
        if i <> 0 then
            b.AddEdge((i - 1, j), (i, j), a.[i].[j])
            b.AddEdge((i, j), (i - 1, j), a.[i - 1].[j])
    b.AddEdgeR(noS, b.AddVertex(i, 0), a.[i].[0])
    b.AddEdgeR(b.AddVertex(i, n - 1), noT, 0)
Graph.shortPath(b.G, noS).[noT] |> printfn "%d"

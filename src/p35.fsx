#load "../lib/Prime.fs"
let n = 1000000
let a = Prime.sieve(n)
type HSS = System.Collections.Generic.HashSet<string>
let ps = HSS()
let mutable i = 2
while i < n do
    ps.Add(string i) |> ignore
    i <- a.[i]
let rotate(s: string, n) =
    let m = n % s.Length
    s.Substring(m) + s.Substring(0, m)
let sieveR(ps: HSS, n) =
    let ret = HSS()
    for i in ps do if ps.Contains(rotate(i, n)) then ret.Add(i) |> ignore
    ret
sieveR(sieveR(sieveR(ps, 4), 2), 1).Count |> printfn "%d"

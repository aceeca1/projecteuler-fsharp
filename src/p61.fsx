#load "../lib/Polygonal.fs"
let a = Array.zeroCreate<int>(6)
let b = [|3; 4; 5; 6; 7; 8|]
let mutable ans = 0
let rec put(m) =
    if m = 6 then
        if a.[5] % 100 = a.[0] / 100 then ans <- a |> Array.sum
    else
        let bm = b.[m]
        let lb = if m = 0 then 5 else m
        for i = 5 downto lb do
            b.[m] <- b.[i]
            b.[i] <- bm
            let lb = if m = 0 then 1000 else a.[m - 1] % 100 * 100
            let ub = if m = 0 then 9999 else lb + 99
            for j = max(1000)(lb) to ub do
                if Polygonal.is(b.[m], int64 j) then
                    a.[m] <- j
                    put(m + 1)
            b.[i] <- b.[m]
put(0)
ans |> printfn "%d"

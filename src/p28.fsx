let n = 500
let a1 = n * (n + 1) * (n + n + 1) / 6
let a2 = n * (n + 1) / 2
(((a1 <<< 2) + a2 + n) <<< 2) + 1 |> printfn "%d"

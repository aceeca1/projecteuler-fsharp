#load "../lib/Prime.fs"
let rec primeLen(a, b, n) =
    if (n + a) * n + b |> Prime.is then primeLen(a, b, n + 1) else n
let mutable ans, iAns = 0, -1
for a = -999 to 999 do
    for b = -1000 to 1000 do
        let c = primeLen(a, b, 0)
        if c > ans then
            ans <- c
            iAns <- a * b
iAns |> printfn "%d"

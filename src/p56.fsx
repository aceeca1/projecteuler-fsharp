let mutable ans = 0
for a = 1 to 100 do
    let mutable s = 1I
    for b = 1 to 100 do
        s <- s * bigint a
        let mutable ds = 0
        for i in string s do
            ds <- ds + (int i - int '0')
        if ds > ans then ans <- ds
ans |> printfn "%d"

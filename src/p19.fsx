let sun = System.DayOfWeek.Sunday
let mutable ans = 0
for i = 1901 to 2000 do
    for j = 1 to 12 do
        let day = System.DateTime(i, j, 1).DayOfWeek
        if day = sun then ans <- ans + 1
ans |> printfn "%d"

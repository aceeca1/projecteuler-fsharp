#load "../lib/Fibonacci.fs"
Fibonacci.startsFrom(1I, 1I) |> Seq.findIndex(
    fun i -> (string i).Length >= 1000) |> (fun i -> i + 1) |> printfn "%d"

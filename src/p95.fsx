#load "../lib/Divisor.fs"
let n = 1000000
let a = Array.zeroCreate<int>(n + 1)
let calc(m) =
    let b = ResizeArray<int>()
    let mutable j = m
    let mutable no = -1
    while j <= n && a.[j] = 0 do
        a.[j] <- no
        b.Add(j) |> ignore
        j <- Divisor.sum(j)
        no <- no - 1
    if j <= n && a.[j] < 0 then
        let cyc = a.[j] - no
        let no1 = ~~~a.[j]
        for k = 0 to no1 - 1 do
            a.[b.[k]] <- System.Int32.MaxValue
        for k = no1 to b.Count - 1 do
            a.[b.[k]] <- cyc
    else
        for k in b do a.[k] <- System.Int32.MaxValue
let mutable ans, iAns = 0, -1
for i = 2 to n do
    if a.[i] = 0 then calc(i)
    if a.[i] <> System.Int32.MaxValue && a.[i] > ans then
        ans <- a.[i]
        iAns <- i
iAns |> printfn "%d"

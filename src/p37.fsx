#load "../lib/Prime.fs"
let mutable ans = 0
let rec put(m) =
    for i in [|1; 3; 7; 9|] do
        let mi = m * 10 + i
        if Prime.isL(mi) then put(mi)
    if (seq {
        let mutable i = 10
        while i < m do
            yield m % i
            i <- i * 10
    }) |> Seq.forall(Prime.isL) then ans <- ans + m
for i in [|2; 3; 5; 7|] do put(i)
ans - 2 - 3 - 5 - 7 |> printfn "%d"

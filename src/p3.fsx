seq {
    let mutable n = 600851475143L
    let mutable m = 2L
    while m * m <= n do
        while n % m = 0L do
            n <- n / m
            yield m
        m <- m + 1L
    if n > 1L then yield n
} |> Seq.last

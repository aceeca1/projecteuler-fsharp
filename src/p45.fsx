#load "../lib/Polygonal.fs"
let mutable ans = 0L
try
    let mutable i = 144
    while true do
        let a = Polygonal.num(6, int64 i)
        if Polygonal.is(5, a) && Polygonal.is(3, a) then
            ans <- a
            failwith "found"
        i <- i + 1
with _ -> ans |> printfn "%d"

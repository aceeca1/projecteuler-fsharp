#load "../lib/Divisor.fs"
let n = 28123
let ab = [| for i = 1 to n do if Divisor.sum(n) > n then yield i |]
let a = System.Collections.BitArray(n + 1)
for i = 0 to ab.Length - 1 do
    let mutable j = i
    while j < ab.Length do
        let s = ab.[i] + ab.[j]
        if s >= a.Length then j <- ab.Length else
            a.Set(s, true)
            j <- j + 1
let mutable ans = 0
for i = 0 to a.Length - 1 do
    if not a.[i] then ans <- ans + i
ans |> printfn "%d"

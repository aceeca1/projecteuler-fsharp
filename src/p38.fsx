let isPan(a: string) =
    let mutable s = 0
    for i in a do s <- s ||| (1 <<< (int i - int '0'))
    s = 0x3fe
let mutable ans = ""
for i = 1 to 9999 do
    let rec f(s: string, k) =
        if s.Length < 9 then f(s + string k, k + i)
        elif s.Length = 9 && isPan(s) then ans <- max(ans)(s)
    f(string i, i + i)
ans |> printfn "%s"

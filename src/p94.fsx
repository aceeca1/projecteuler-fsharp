(* Let p be half of the unique edge, and we have
    (2p +- 1)^2 - p^2 = (s / p)^2 .
Then (s / p) must be integer. So,
    (2p +- 1)^2 - p^2 = h^2 .
Rewrite it to get pell equations,
    (3p +- 2)^2 - 3h^2 = 1 .
Use wolframalpha.com to get the solutions. *)
open System.Linq
let sqrt3 = sqrt(3.0)
let b = 7.0 + 4.0 * sqrt3
let se1 = seq {
    let mutable a = (2.0 - sqrt3) * b
    while true do
        a <- a * b
        yield a - 2.0 |> round |> int64
}
let se2 = seq {
    let mutable a = 1.0
    while true do
        a <- a * b
        yield a + 2.0 |> round |> int64
}
let n = 1000000000L
let ans1 = se1.TakeWhile(fun i -> i <= n).Sum()
let ans2 = se2.TakeWhile(fun i -> i <= n).Sum()
ans1 + ans2 |> printfn "%d"

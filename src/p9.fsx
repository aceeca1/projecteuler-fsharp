let mutable ans = 0
let mutable a = 1
while a <= 1000 && ans = 0 do
    let s1 = 1000 - a
    let s2 = a * a
    if s2 % s1 = 0 then
        let b2 = s1 - s2 / s1
        if b2 < 0 || b2 &&& 1 = 0 then
            let b = b2 >>> 1
            let c = s1 - b
            ans <- a * b * c
    a <- a + 1
ans |> printfn "%d"

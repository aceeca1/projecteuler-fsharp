let choose(n, k) =
    let mutable ret = 1L
    for i = 1 to k do ret <- ret * int64(n - i + 1) / int64 i
    ret
choose(40, 20) |> printfn "%d"

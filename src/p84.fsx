#load "../lib/Matrix.fs"
let n = 40
let a = Array.zeroCreate<double>(n)
for i = 1 to 4 do
    for j = 1 to 4 do
        a.[i + j] <- a.[i + j] + 1.0 / 16.0
let threeDoubleJail = 3.0 / 1024.0
for i in 2 .. 2 .. 8 do a.[i] <- a.[i] - threeDoubleJail
let mutable b = Array2D.zeroCreate<double>(n)(n)
for i = 0 to n - 1 do
    for j = 0 to n - 1 do
        let diff = (j - i + n) % n
        b.[i, j] <- a.[diff]
        if j = 10 then b.[i, j] <- b.[i, j] + threeDoubleJail * 4.0
let sp = [|
    30, Array.create(16)(10)
    7,  [|0; 10; 11; 24; 39; 5; 15; 15; 12; 4 |]
    22, [|0; 10; 11; 24; 39; 5; 25; 25; 28; 19|]
    36, [|0; 10; 11; 24; 39; 5; 5;  5;  12; 33|]
    2,  [|0; 10|]
    17, [|0; 10|]
    33, [|0; 10|] |]
for k, v in sp do
    for i = 0 to n - 1 do
        let share = b.[i, k] / 16.0
        b.[i, k] <- share * float(16 - v.Length)
        for j in v do b.[i, j] <- b.[i, j] + share
for i = 1 to 30 do b <- Matrix.multiply(b, b)
let c = [| 0 .. n - 1 |] |> Seq.sortBy(fun i -> -b.[0, i])
c |> Seq.take(3) |> System.String.Concat |> printfn "%s"

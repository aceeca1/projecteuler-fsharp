#load "../lib/Fibonacci.fs"
let mutable ans = 0
for i in Fibonacci.startsFrom(1, 1) |> Seq.takeWhile(fun i -> i <= 4000000) do
    if i &&& 1 = 0 then ans <- ans + i
ans |> printfn "%d"

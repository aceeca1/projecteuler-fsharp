let mutable ans, pAns = 0, -1
for p = 1 to 1000 do
    let mutable s = 0
    for a = 1 to p - 1 do
        let a2 = a * a
        let bpc = p - a
        if a2 % bpc = 0 then
            let cmb = a2 / bpc
            let bb = bpc - cmb
            if bb &&& 1 = 0 then
                let b = bb >>> 1
                if a < b then s <- s + 1
    if s > ans then ans <- s; pAns <- p
pAns |> printfn "%d"

let a = Array.zeroCreate<int>(10)
try
    let mutable s = 0
    let rec put(m) =
        if m >= 10 then failwith "found" else
            let ub = if m = 0 then 6 else 9
            let lb =
                match m with
                | 3 | 5 | 7 | 9 -> a.[0]
                | _ -> 1
            for am = ub downto lb do
                a.[m] <-
                    match m with
                    | 3 | 5 | 7 | 9 -> if am = lb then 10 else am
                    | _ -> am
                if
                    (seq {0 .. m - 1})
                    |> Seq.forall(fun i -> a.[i] <> a.[m]) then
                    match m with
                    | 2 ->
                        s <- a.[0] + a.[1] + a.[2]
                        put(m + 1)
                    | 4 | 6 | 8 ->
                        if s = a.[m] + a.[m - 1] + a.[m - 2] then put(m + 1)
                    | 9 ->
                        if s = a.[1] + a.[8] + a.[9] then put(m + 1)
                    | _ -> put(m + 1)
    put(0)
with _ ->
    printfn "%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d"
        a.[0] a.[1] a.[2] a.[3] a.[2] a.[4]
        a.[5] a.[4] a.[6] a.[7] a.[6] a.[8]
        a.[9] a.[8] a.[1]


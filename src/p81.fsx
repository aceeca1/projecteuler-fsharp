let file = new System.IO.StreamReader("../data/input81.txt")
let n = 80
let a = Array.init(n)(fun i -> file.ReadLine().Split(',') |> Array.map(int))
let b = Array.create(n)(System.Int32.MaxValue)
b.[0] <- 0
for ai in a do
    b.[0] <- b.[0] + ai.[0]
    for j = 1 to n - 1 do
        if b.[j - 1] < b.[j] then b.[j] <- ai.[j] + b.[j - 1]
        else b.[j] <- ai.[j] + b.[j]
b.[n - 1] |> printfn "%d"

let n = 100
let a1 = n * (n + 1) >>> 1
let a2 = n * (n + 1) * (n + n + 1) / 6
a1 * a1 - a2 |> printfn "%d"

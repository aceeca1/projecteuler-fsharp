let next(n) = if n &&& 1L = 0L then n >>> 1 else n * 3L + 1L
let a = Array.zeroCreate<int>(1000001)
a.[1] <- 1
let rec c(n) =
    if n > 1000000L then c(next(n)) + 1
    elif a.[int n] <> 0 then a.[int n] else
        let ans = c(next(n)) + 1
        a.[int n] <- ans
        ans
let mutable ans = 0
for i = 1 to 1000000 do if c(int64 i) > a.[ans] then ans <- i
ans |> printfn "%d"

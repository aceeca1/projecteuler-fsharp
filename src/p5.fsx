#load "../lib/Divisor.fs"
let mutable ans = 1
for i = 1 to 20 do ans <- ans * (i / Divisor.gcd(ans, i))
ans |> printfn "%d"

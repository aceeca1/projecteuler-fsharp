let fac = Array.zeroCreate<int>(10)
fac.[0] <- 1
for i = 1 to 9 do fac.[i] <- fac.[i - 1] * i
let mutable ans = 0
for i = 10 to fac.[9] * 7 do
    let mutable s = 0
    for j in string i do s <- s + fac.[int j - int '0']
    if s = i then ans <- ans + i
ans |> printfn "%d"

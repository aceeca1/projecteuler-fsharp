#load "../lib/Prime.fs"
let num = 5
[<AllowNullLiteral>]
type PPSet(v, parent: PPSet) =
    let vS = string v
    let n = if isNull parent then 1 else parent.N + 1
    member u.V = v
    member u.Parent = parent
    member u.VS = vS
    member u.N = n
    member val Prio =
        if isNull parent then v * num
        else parent.Prio + (v - parent.V) * (num + 1 - n)
    member val Valid =
        (seq {
            let mutable i = parent
            while isNull i |> not do
                for j in [|i.VS + vS; vS + i.VS|] do
                    yield Prime.isL(int j)
                i <- i.Parent
        }) |> Seq.forall(id)
    interface System.IComparable<PPSet> with
        member u1.CompareTo(u2) =
            compare (u1.Prio, hash u1) (u2.Prio, hash u2)
let mutable ans = 0
let qu = System.Collections.Generic.SortedSet<PPSet>()
qu.Add(PPSet(2, null)) |> ignore
let mutable break1 = false
while qu.Count <> 0 && not break1 do
    let quMin = qu.Min
    qu.Remove(quMin) |> ignore
    if not quMin.Valid then ()
    elif quMin.N = num then
        ans <- quMin.Prio
        break1 <- true
    else qu.Add(PPSet(quMin.V |> Prime.next, quMin)) |> ignore
    qu.Add(PPSet(quMin.V |> Prime.next, quMin.Parent)) |> ignore
ans |> printfn "%d"

let a = System.Collections.Generic.HashSet<int>()
for i in [|1; 10; 100; 1000; 10000; 100000; 1000000|] do a.Add(i) |> ignore
let mutable ans = 1
let mutable c = 1
let mutable n = 1
while c <= 1000000 do
    for i in string n do
        if a.Contains(c) then ans <- ans * (int i - int '0')
        c <- c + 1
    n <- n + 1
ans |> printfn "%d"

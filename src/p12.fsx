#load "../lib/Divisor.fs"
let mutable n = 1
let mutable m = 2
while Seq.length(Divisor.ofNum(n)) <= 500 do
    n <- n + m
    m <- m + 1
n |> printfn "%d"

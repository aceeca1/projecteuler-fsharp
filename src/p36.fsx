let isPalindrome(n: int, k: int) =
    let s = System.Convert.ToString(n, k)
    let a = s.ToCharArray()
    System.Array.Reverse(a)
    a = s.ToCharArray()
let mutable ans = 0
for i = 1 to 999999 do
    if isPalindrome(i, 10) && isPalindrome(i, 2) then
        ans <- ans + i
ans |> printfn "%d"

open System.Linq
let file = new System.IO.StreamReader("../data/input79.txt")
let n = 50
let a = Array.init(n)(fun i -> file.ReadLine())
[<AllowNullLiteral>]
type Node(v, parent:Node) =
    let pc = if isNull parent then "" else parent.Pc + string v
    let c =
        if isNull parent then Array.zeroCreate<int>(n)
        else Array.init(n)(fun i ->
            if
                parent.C.[i] = a.[i].Length ||
                a.[i].[parent.C.[i]] <> v
            then parent.C.[i] else parent.C.[i] + 1)
    let m1 =
        (seq {0 .. n - 1})
        |> Seq.map(fun i -> a.[i].Length - c.[i]) |> Seq.max
    let m2 =
        let b = System.Collections.BitArray(10)
        for i = 0 to n - 1 do
            for j = c.[i] to a.[i].Length - 1 do
                b.[int a.[i].[j] - int '0'] <- true
        (seq {0 .. 9}).Count(fun i -> b.[i])
    member u.Pc = pc
    member u.C: int[] = c
    member val Prio = pc.Length + max(m1)(m2)
    member val S =
        (seq {0 .. n - 1})
        |> Seq.map(fun i -> a.[i].Length - c.[i]) |> Seq.sum
    member u.Childs = seq {
        for i in '0' .. '9' do
            let ch = Node(i, u)
            if ch.S < u.S then yield ch
    }
let mutable ans, iAns = System.Int32.MaxValue, (null: string)
let rec put(node: Node) =
    if node.S = 0 then
        if node.Pc.Length < ans then
            ans <- node.Pc.Length
            iAns <- node.Pc
    else
        for i in node.Childs do
            if i.Prio < ans then put(i)
put(Node(' ', null))
iAns |> printfn "%s"

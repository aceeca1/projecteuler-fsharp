let file = new System.IO.StreamReader("../data/input11.txt")
let n = 20
let a = Array.init(n)(fun i -> file.ReadLine().Split() |> Array.map(int))
let dX = [| 0; 1; 1;  1 |]
let dY = [| 1; 0; 1; -1 |]
let prod(x, y, d) =
    let rec f(m, x, y, ans) =
        if m = 4 then ans
        elif not((0 <= x && x < n) && (0 <= y && y < n)) then 0 else
            f(m + 1, x + dX.[d], y + dY.[d], ans * a.[x].[y])
    f(0, x, y, 1)
let mutable ans = 0
for i = 0 to n - 1 do
    for j = 0 to n - 1 do
        for k = 0 to 3 do
            ans <- max(ans)(prod(i, j, k))
ans |> printfn "%d"

open System.Linq
let e = seq {
    yield 2
    let mutable i = 2
    while true do
        yield 1
        yield i
        yield 1
        i <- i + 2
}
let e100 = e.Take(100).ToArray()
System.Array.Reverse(e100)
let mutable numer, denom = 1I, 0I
for i in e100 do
    let numer0 = numer
    numer <- numer * bigint i + denom
    denom <- numer0
let mutable ans = 0
for i in string numer do ans <- ans + (int i - int '0')
ans |> printfn "%d"

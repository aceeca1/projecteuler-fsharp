let n = 100
let a = Array.zeroCreate<int>(n + 1)
a.[0] <- 1
for i = 1 to n do
    for j = i to n do a.[j] <- a.[j] + a.[j - i]
a.[n] - 1 |> printfn "%d"

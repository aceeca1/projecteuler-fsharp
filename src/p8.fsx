let file = new System.IO.StreamReader("../data/input8.txt")
let sb = System.Text.StringBuilder()
for i = 1 to 20 do sb.Append(file.ReadLine()) |> ignore
let s = sb.ToString()
type ProdZ() =
    let mutable pd = 1L
    let mutable z = 0
    member u.Add(c) =
        if c = '0' then z <- z + 1 else
            pd <- pd * (int64 c - int64 '0')
    member u.Del(c) =
        if c = '0' then z <- z - 1 else
            pd <- pd / (int64 c - int64 '0')
    member u.Pd = if z = 0 then pd else 0L
let pz = ProdZ()
let mutable ans = 0L
for i = 0 to 12 do pz.Add(s.[i])
for i = 13 to s.Length do
    ans <- max(ans)(pz.Pd)
    if i <> s.Length then
        pz.Add(s.[i])
        pz.Del(s.[i - 13])
ans |> printfn "%d"

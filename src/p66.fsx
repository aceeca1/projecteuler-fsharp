let pell(n:int) =
    let isSolution(p, q) = p * p - q * q * bigint n = 1I
    let k = n |> float |> sqrt |> int
    if k * k = n then (0I, 0I) else
        let p0, q0 = bigint k, bigint 1
        if isSolution(p0, q0) then (p0, q0) else
            let rec f(p0, q0, p1, q1, u1, v1, a1) =
                if isSolution(p1, q1) then (p1, q1) else
                    let u2 = a1 * v1 - u1
                    let v2 = (n - u2 * u2) / v1
                    let a2 = (k + u2) / v2
                    let p2 = bigint a2 * p1 + p0
                    let q2 = bigint a2 * q1 + q0
                    f(p1, q1, p2, q2, u2, v2, a2)
            let u1 = k
            let v1 = n - k * k
            let a1 = (k + k) / v1
            let p1 = bigint k * bigint a1 + 1I;
            let q1 = bigint a1
            f(p0, q0, p1, q1, u1, v1, a1)
let mutable ans, iAns = 0I, -1
for i = 1 to 1000 do
    let p, q = pell(i)
    if p > ans then
        ans <- p
        iAns <- i
iAns |> printfn "%d"

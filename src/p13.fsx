let file = new System.IO.StreamReader("../data/input13.txt")
let a = Seq.init(100)(fun i -> file.ReadLine() |> bigint.Parse)
(a |> Seq.sum |> string).[.. 9] |> printfn "%s"

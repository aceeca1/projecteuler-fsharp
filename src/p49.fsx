#load "../lib/Hash.fs"
#load "../lib/Prime.fs"
let u = System.Collections.Generic.Dictionary<int64, ResizeArray<int>>()
let a = Prime.sieve(10000)
let mutable i = 1000
while a.[i] < i do i <- i + 1
while i < 10000 do
    let mutable h = 0L
    for j in string i do h <- h + Hash.h64(int64 j)
    if u.ContainsKey(h) |> not then u.Add(h, ResizeArray<int>())
    u.[h].Add(i) |> ignore
    i <- a.[i]
for ui in u do
    let uiV = ui.Value
    for i1 = 0 to uiV.Count - 1 do
        for i2 = i1 + 1 to uiV.Count - 1 do
            for i3 = i2 + 1 to uiV.Count - 1 do
                if uiV.[i2] - uiV.[i1] = uiV.[i3] - uiV.[i2] then
                    if uiV.[i1] <> 1487 then
                        printfn "%d%d%d" uiV.[i1] uiV.[i2] uiV.[i3]

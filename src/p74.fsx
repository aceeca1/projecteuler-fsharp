let fac = Array.zeroCreate<int>(10)
fac.[0] <- 1
for i = 1 to 9 do fac.[i] <- fac.[i - 1] * i
let a = Array.zeroCreate<int>(fac.[9] * 7 + 1)
let next(n: int) =
    let mutable ret = 0
    for i in string n do ret <- ret + fac.[int i - int '0']
    ret
let n = 1000000
let mutable ans = 0
for i = 1 to n - 1 do
    if a.[i] = 0 then
        let b = ResizeArray<int>()
        let mutable j = i
        let mutable no = -1
        while a.[j] = 0 do
            a.[j] <- no
            b.Add(j) |> ignore
            j <- next(j)
            no <- no - 1
        let no1 = if a.[j] > 0 then ~~~no else ~~~a.[j]
        let mutable terms = if a.[j] > 0 then a.[j] else a.[j] - no
        for j = b.Count - 1 downto 0 do
            if j < no1 then terms <- terms + 1
            a.[b.[j]] <- terms
    if a.[i] = 60 then ans <- ans + 1
ans |> printfn "%d"

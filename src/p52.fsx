#load "../lib/Hash.fs"
let digitHash(n) =
    let mutable ret = 0L
    for i in string n do ret <- ret + Hash.h64(int64 i)
    ret
let isPermuteMul(n) =
    let h = digitHash(n)
    (seq {2 .. 6}) |> Seq.forall(fun i -> digitHash(i * n) = h)
let mutable i = 1
while isPermuteMul(i) |> not do i <- i + 1
i |> printfn "%d"

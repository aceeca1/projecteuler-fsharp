#load "../lib/Prime.fs"
let n = 10000
let m = float n * log(float n) * 1.5 |> int
let a = Prime.sieve(m)
let mutable ans = 2
for i = 2 to 10001 do ans <- a.[ans]
ans |> printfn "%d"

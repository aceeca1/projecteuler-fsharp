#load "../lib/Prime.fs"
let rec f(n, h) =
    if h = 4 then n - 4 else
        let len = Prime.factorize(n) |> Seq.distinct |> Seq.length
        f(n + 1, if len = 4 then h + 1 else 0)
f(1, 0) |> printfn "%d"

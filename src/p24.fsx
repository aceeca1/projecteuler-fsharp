let a = Array.zeroCreate<int>(10)
let mutable n = 1000000 - 1
for i = 1 to 10 do
    a.[10 - i] <- n % i
    n <- n / i
let allBut(a: int[], m) =
    Array.init(a.Length - 1)(fun i ->
        if i < m then a.[i] else a.[i + 1])
let mutable av = [|0 .. 9|]
for i = 0 to 9 do
    let ai = a.[i]
    printf "%d" av.[ai]
    av <- allBut(av, ai)
printfn ""

let mutable ans = 0
try
    let mutable ub = 1000
    while true do
        let a = Array.zeroCreate<int>(ub + 1)
        a.[0] <- 1
        for i = 1 to ub do
            for j = i to ub do
                a.[j] <- (a.[j] + a.[j - i]) % 1000000
        for i = 1 to ub do
            if a.[i] = 0 then
                ans <- i
                failwith "found"
        ub <- ub + ub
with _ -> ans |> printfn "%d"
